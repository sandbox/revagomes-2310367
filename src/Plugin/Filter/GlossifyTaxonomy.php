<?php
/**
 * @file
 * Contains \Drupal\glossify\Plugin\Filter\GlossifyTaxonomy.
 */

namespace Drupal\glossify\Plugin\Filter;

use Drupal\filter\Annotation\Filter;
use Drupal\Core\Annotation\Translation;
use Drupal\filter\Plugin\FilterBase;

/**
 * Provides a filter to link taxonomy terms appearing in content
 * to their taxonomy term page.
 *
 * @Filter(
 *   id = "glossify_taxonomy",
 *   title = @Translation("Glossify with taxonomy"),
 *   type = FILTER_TYPE_HTML_RESTRICTOR,
 *   settings = {
 *     "allowed_html" = "<a> <em> <strong> <cite> <blockquote> <code> <ul> <ol> <li> <dl> <dt> <dd> <h4> <h5> <h6>",
 *     "filter_html_help" = 1,
 *     "filter_html_nofollow" = 0
 *   },
 *   weight = -10
 * )
 */
class GlossifyTaxonomy extends FilterBase {

  /**
   * The settings for configuring the glossify taxonomy filter.
   */
  public function settingsForm(array $form, array &$form_state) {
    $filter->settings += $defaults;
    $vocabs = array();

    foreach (taxonomy_get_vocabularies() as $vocab) {
      $vocabs[$vocab->vid] = $vocab->name;
    }

    $settings['glossify_taxonomy_case_sensitivity'] = array(
      '#type' => 'checkbox',
      '#title' => t('Case sensitive'),
      '#description' => t('Whether or not the match is case sensitive.'),
      '#default_value' => $filter->settings['glossify_taxonomy_case_sensitivity'],
    );
    $settings['glossify_taxonomy_first_only'] = array(
      '#type' => 'checkbox',
      '#title' => t('First match only'),
      '#description' => t('Match and link only the first occurance.'),
      '#default_value' => $filter->settings['glossify_taxonomy_first_only'],
    );
    $settings['glossify_taxonomy_tooltips'] = array(
      '#type' => 'checkbox',
      '#title' => t('Tool tips'),
      '#description' => t('Enable tooltips displaying the taxonomy term description when hovering the link.'),
      '#default_value' => $filter->settings['glossify_taxonomy_tooltips'],
    );
    $settings['glossify_taxonomy_vocabs'] = array(
      '#type' => 'checkboxes',
      '#title' => t('Taxonomy vocabularies'),
      '#description' => t('Select the taxonomy vocabularies who\'s terms should be linked to their term page.'),
      '#options' => $vocabs,
      '#default_value' => $filter->settings['glossify_taxonomy_vocabs'],
    );

    return $settings;
  }

  public function process($text, $langcode, $cache, $cache_id) {
    //get vocabularies
    $vocabs = array_filter($filter->settings['glossify_taxonomy_vocabs']);
    if (!$vocabs) return $text;

    //get terms
    $result = db_query("SELECT tid AS id, name, LOWER(name) AS name_norm, description AS tip FROM {taxonomy_term_data} WHERE vid IN (:vids)", array(':vids' => $vocabs));
    $terms = $result->fetchAllAssoc('name_norm');

    //process text
    if (count($terms) > 0) {
      return _glossify_to_links($text, $terms, 'taxonomy', $filter->settings['glossify_taxonomy_case_sensitivity'], $filter->settings['glossify_taxonomy_first_only'], $filter->settings['glossify_taxonomy_tooltips']);
    }
    else {
      return $text;
    }
  }
}
